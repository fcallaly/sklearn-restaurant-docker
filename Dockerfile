FROM python:3.8

COPY ./app app
COPY run.sh run.sh
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN python3 -c "import nltk; nltk.download('stopwords')"

EXPOSE 5000
ENTRYPOINT ["/bin/sh", "run.sh"]

